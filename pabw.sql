-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 14 Jan 2019 pada 11.44
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pabw`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `mhs`
--

CREATE TABLE `mhs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nim` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_mhs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fakultas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prodi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thn_masuk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notelp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `mhs`
--

INSERT INTO `mhs` (`id`, `nim`, `nama_mhs`, `jk`, `fakultas`, `prodi`, `thn_masuk`, `alamat`, `notelp`, `created_at`, `updated_at`) VALUES
(4, '151080200091', 'FITRI INAYAH', 'P', 'Pendidikan', 'INFORMATIKA', '2015', 'SIDOARJO', '08787746678', NULL, '2018-12-30 20:49:01'),
(5, '151080200173', 'ROIS ULA', 'P', 'Pendidikan', 'PENDIDIKAN AGAMA ISLAM', '2015', 'SIDOARJO', '087878898876', '2019-01-10 09:09:04', '2019-01-10 09:09:37'),
(6, '171727272727', 'SOKRAN', 'L', 'Teknik', 'TEKNIK MESIN', '2017', 'SIDOARJO', '8979797978', '2019-01-10 10:41:49', '2019-01-10 10:41:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_12_28_220037_create_mhs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT 'avatar5.png',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `role`, `email`, `email_verified_at`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'admin@jarangturu.xyz', NULL, '$2y$10$eWq3Lq4/x4XeTILuk2rbOOET2FRLx5FCWK212cVJbd9eYbrLiI8vG', 'AsAtG9lUZx5y8IhRPLopvxPPtZEQXr.png', 'A6MIkzedS0R6tMmbv0y9GDMhi8fcdvUo26Y82bYjtniKsgj7kV1v436XpXCd', '2018-12-30 22:11:02', '2019-01-12 14:58:27'),
(2, 'ABINURDIAN, S.Kom., M.Kom.', 'dosen', 'abinurdian@jarangturu.xyz', NULL, '$2y$10$vvcAmbGDGV9LPGaQFTiP.uJdEx95ErplzPP6qqNQpm5UYAVCLUHRW', '', 'A90wP8nTzcDteWTSxN9Ekqoq9KPagtpbFzCdnJLhN8wI6UMNHquVzSxZ4im9', '2018-12-30 23:08:05', '2018-12-30 23:08:05'),
(3, 'M AFANDARY', 'admin', 'afan@jarangturu.xyz', NULL, '$2y$10$xfLmnMKWiXPt7kwFmWagy.Xh/zMSCdyVHMPZtDZbriGE5OG4LR/e2', '', '3afi1RpwmTf2ZLga2YJgCD4XkfiIC4AAPqtLcU513G4RrWScWgreWA37te0S', '2019-01-10 09:05:37', '2019-01-10 09:05:37'),
(5, 'rois', 'mahasiswa', 'rois@gm.com', NULL, '$2y$10$M5Pvb3WAdk5nIl9ZtlYY4OpDV.ay5.D0bFvQSut5g9bWVjheQicui', '', '78pwh4shseIa3kDjDTMiXjW0xdSqlzaoovInofVd3XTtLmKzxghINvtYL5Qr', '2019-01-10 09:09:04', '2019-01-10 09:09:04'),
(6, 'SOKRAN', 'mahasiswa', 'sokran@gmail.com', NULL, '$2y$10$hwu/8Py5L3o1KYp9k9MjfupxIxtYCplY/S149s6KLV4eWg6a/IqLG', '76DmdKHH6x14OirRNTR0Q3vXde2wg5.png', 'vD3vpbZlxVSnTGVE90ny39ha8cUIZbItxPhDkNSlqYkqjkJdSNdU6cCqCieU', '2019-01-10 10:41:49', '2019-01-11 02:28:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mhs`
--
ALTER TABLE `mhs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mhs_nim_unique` (`nim`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mhs`
--
ALTER TABLE `mhs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
