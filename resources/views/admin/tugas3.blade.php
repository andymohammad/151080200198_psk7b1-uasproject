@extends('layout.master')
@section('konten')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Base64
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
    <!-- /.box-header -->
    <form role="form" id="form_gbr" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
    <div class="box-body">
        <div class="box-body">
            <div class="form-group">
                <label>File input</label>
                <input type="file" name="gambar" id="gambar">
            </div>
            <div class="form-group">
                <div class="form-group">
                <label>Hasil Konversi Gambar ke String</label>
                <button type="submit" class="btn btn-success pull-right">Upload</button>
            </div>
            </div>
    </form>
    <textarea class="form-control" rows="16" name="hasil" id="hasil"></textarea>
  </div>
</section>
@endsection

@section('jskonten')
<script type="text/javascript">
    $(document).on('submit', '#form_gbr', function(e){
        e.preventDefault();

        var form_data = new FormData($('#form_gbr')[0]);
        $.ajax({
            type: 'POST',
            url: '{{ route('ambil') }}',
            processData: false,
            contentType: false,
            async: false,
            cache: false,
            data: form_data,
            success: function(response){
                $('#hasil').val(response.data);
            }
        });
    });
</script>
@endsection
