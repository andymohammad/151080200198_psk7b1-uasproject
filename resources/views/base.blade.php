@extends('layout.master')
@section('konten')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Base64 Encoding
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
    <!-- /.box-header -->
    <form role="form" name="form_gbr" action="/ambilgambar" enctype="multipart/form-data" method="POST">
        {{ csrf_field() }}
    <div class="box-body">
        <div class="box-body">
            <div class="form-group">
                <label>File input</label>
                <button type="submit" name="image" id="simpan" class="btn btn-success pull-right">Upload</button>
                <input type="file" name="image" id="file">
            </div>
            <div class="form-group">
                <div class="form-group">
                <label>Hasil Konversi Gambar ke String</label>
                <textarea class="form-control" rows="14" name="hasil" id="hasil" disabled>{{$image}}</textarea>
                </div>
            </div>
    </form>
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
@endsection
