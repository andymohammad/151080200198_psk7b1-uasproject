@extends('layout.master')
@section('konten')

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Data Mahasiswa
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
    <!-- /.box-header -->
    <div class="box-body">

      @foreach ($detail as $d)
        <div class="box-body no-padding">
            <table class="table table-striped">
              <tr>
                <th>NAMA</th>
                <th>NIM</th>
                <th>FAKULTAS</th>
                <th>PRODI</th>
                <th>ANGKATAN</th>
                <th>ALAMAT</th>
              </tr>
              <tr>
                <td><span class="badge bg-green"> {{ $d->nama_mhs }} </span></td>
                <td>{{ $d->nim }}</td>
                <td>{{ $d->fakultas }}</td>
                <td> {{ $d->prodi }} </td>
                <td> {{ $d->thn_masuk }}</td>
                <td> {{ $d->alamat }} </td>
              </tr>
              @endforeach
            </table>
        </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->
      </div>
</section>

@endsection

@section('jskonten')
<!-- DataTables -->
<script src="/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
@endsection
