<?php
Route::get('/', function () {
    return view('auth.login');
});

Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');

Route::get('logout', 'AuthController@logout')->middleware('auth','checkRole:admin,mahasiswa');
Route::get('/dashboard', 'AdminController@dashboard')->middleware('auth','checkRole:admin,mahasiswa');

Route::get('/profile', 'UserController@profile');
Route::post('profile', 'UserController@uploadfoto');
Route::get('/dashboard/detail/{parameter}', 'MahasiswaController@detailmhs')->middleware('auth', 'checkRole:admin,mahasiswa,dosen');


Route::get('/gambarkestring', 'UploadGambarController@index');
Route::post('/ambilgambar', 'UploadGambarController@coba')->name('ambil');


Auth::routes();
//user route
//Route::get('users/{user}',  ['as' => 'user.edit', 'uses' => 'UserController@edit']);
//Route::patch('user/{user}/update',  ['as' => 'users.update', 'uses' => 'UserController@update']);

Route::get('/datamhs', 'MahasiswaController@datamhs')->middleware('auth','checkRole:admin');
Route::post('/datamhs/create', 'MahasiswaController@create')->middleware('auth', 'checkRole:admin');
Route::get('/datamhs/{id}/edit', 'MahasiswaController@edit')->middleware('auth', 'checkRole:admin');
Route::post('/datamhs/{id}/update', 'MahasiswaController@update')->middleware('auth', 'checkRole:admin');
Route::get('/datamhs/{id}/delete', 'MahasiswaController@delete')->middleware('auth', 'checkRole:admin');

Route::get('/home', 'HomeController@index')->name('home');
