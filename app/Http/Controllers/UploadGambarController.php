<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadGambarController extends Controller
{
    public function index()
    {
        return view('admin.tugas3')->with(['data'=>"hasil"]);
    }

    public function coba(Request $request)
    {
        $gb = $request->file('gambar');
        $image = base64_encode(file_get_contents($gb));

        $data = [
            'data' => $image
        ];
        return response()->json($data);
        //return view('base', ['image' => $image]);
    }
}
